from boto3.dynamodb.conditions import Key
import boto3
import os
import datetime
import time

dynamodb = boto3.client('dynamodb')
dynamodb_table = os.environ["DYNAMODB_TABLE_NAME"]
dynamodbr = boto3.resource('dynamodb')
table = dynamodbr.Table(dynamodb_table)

def Delete_Item(Mydict):
    try:
        response = table.delete_item(
                         Key={ 'name': Mydict['pkey'], 'city': Mydict['skey'] }
                   )
    except Exception as e:
         print('Error performing Update_Item')
         print(e)
         response = e
    return response
    
def PutItem(Mydict):
    try:
         response = table.put_item(Item=Mydict)
    except Exception as e:
         print('Error performing Put_Item')
         print(e)
         response = e
    return response

def Query(pkey):
    try:
         response = table.query(KeyConditionExpression=Key('name').eq(pkey))
    except Exception as e:
         print('Error performing Query')
         print(e)
         response = e
    return response

def QuerywithSortKey(pkey,skey):
    try:
         response = table.query(KeyConditionExpression=Key('name').eq(pkey) & Key('city').eq(skey))
    except Exception as e:
         print('Error performing Query')
         print(e)
         response = e
    return response
    
def QueryIndex(index_name,index,pkey):
    try:
         response = table.query(IndexName=index_name, KeyConditionExpression=Key(index).eq(pkey))
    except Exception as e:
         print('Error performing Query')
         print(e)
         response = e
    return response
    

def Update_Item(Mydict):
    updateexpression = 'SET ' + '#zz' + ' = :value'
    try:
         response = table.update_item(
                          Key={ 'name': Mydict['pkey'], 'city': Mydict['skey'] },
                          UpdateExpression=updateexpression,
                          ExpressionAttributeValues={ ':value': Mydict["value"]},
                          ExpressionAttributeNames={ '#zz': Mydict["key"] }
                    )
    except Exception as e:
         print('Error performing Update_Item')
         print(e)
         response = e
    return response

