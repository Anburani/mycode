import Dynamodb

def lambda_handler(event, context):
    Myitems = {}
    ## INSERT
    '''
    Myitems["name"] = "Jansi"
    Myitems["city"] = "Chennai"
    Myitems["college"] = "KM"
    Myitems["game"] = "Kabadi"
    Myitems["hobby"] = "facebook"
    try:
        response = Dynamodb.PutItem(Myitems)
        print(response)
    except Exception as e:
        print(e)
        '''
        
    ## UPDATE
    '''
    Myitems["pkey"] = "Jansi"
    Myitems["skey"] = "Chennai"
    Myitems["key"] = "college"
    Myitems["value"] = "Quaid-E-Millath"
    try:
        response = Dynamodb.Update_Item(Myitems)
    except Exception as e:
        print(e)
        '''
        
    ## DELETE
    '''
    Myitems["pkey"] = "Jansi"
    Myitems["skey"] = "Chennai"
    try:
        response = Dynamodb.Delete_Item(Myitems)
    except Exception as e:
        print(e)
        '''
        
    ## QUERY
    #print(Dynamodb.Query("jansi"))
    #print(Dynamodb.QuerywithSortKey("jansi","chennai"))
    
    ## Query with Index
    #print(Dynamodb.QueryIndex("City","city","chennai"))
    
